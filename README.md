# React with Rails 5 API's
To integrate react app with rails using web packer,

## 	Library
`ruby version 2.5.5`

`rails version 5.2.3`

## Installation
`bundle install`

`rails webpacker:install`

`rails webpacker:install:react`

`rails db:create`

`rails db:migrate`

`rails db:seed`

## Run application
`foreman start -f Procfile.dev -p 3000`